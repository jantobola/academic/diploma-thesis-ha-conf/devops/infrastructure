resource "aws_instance" "question-service" {
  ami  = "ami-02d2e9a2fd837fd50"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.diploma-key.id}"
  subnet_id = "${aws_subnet.public-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.diploma-public-sg.id}"]
  associate_public_ip_address = true
  source_dest_check = false
  tags {
    Name = "question-service"
  }
}

resource "aws_instance" "messaging-service" {
  ami  = "ami-02d2e9a2fd837fd50"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.diploma-key.id}"
  subnet_id = "${aws_subnet.public-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.diploma-public-sg.id}"]
  associate_public_ip_address = true
  source_dest_check = false
  tags {
    Name = "messaging-service"
  }
}

resource "aws_instance" "config-server" {
  ami  = "ami-02d2e9a2fd837fd50"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.diploma-key.id}"
  subnet_id = "${aws_subnet.public-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.diploma-public-sg.id}"]
  associate_public_ip_address = true
  source_dest_check = false
  tags {
    Name = "config-server"
  }
}

resource "aws_instance" "eureka" {
  ami  = "ami-02d2e9a2fd837fd50"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.diploma-key.id}"
  subnet_id = "${aws_subnet.public-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.diploma-public-sg.id}"]
  associate_public_ip_address = true
  source_dest_check = false
  tags {
    Name = "eureka"
  }
}

resource "aws_instance" "sba" {
  ami  = "ami-02d2e9a2fd837fd50"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.diploma-key.id}"
  subnet_id = "${aws_subnet.public-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.diploma-public-sg.id}"]
  associate_public_ip_address = true
  source_dest_check = false
  tags {
    Name = "sba"
  }
}

resource "aws_instance" "postgres" {
  ami  = "ami-02d2e9a2fd837fd50"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.diploma-key.id}"
  subnet_id = "${aws_subnet.public-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.diploma-public-sg.id}", "${aws_security_group.postgres-sg.id}"]
  associate_public_ip_address = true
  source_dest_check = false
  tags {
    Name = "postgres"
  }
  provisioner "remote-exec" {
    inline = [
      "docker run -d --restart=always -e 'POSTGRES_USER=postgres' -e 'POSTGRES_PASSWORD=postgres' --network host toby91/diploma:postgres.latest"
    ]
  }
  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = "${file("~/.ssh/diploma/id_rsa")}"
  }
}

resource "aws_instance" "kafka" {
  ami  = "ami-02d2e9a2fd837fd50"
  instance_type = "t2.small"
  key_name = "${aws_key_pair.diploma-key.id}"
  subnet_id = "${aws_subnet.public-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.diploma-public-sg.id}", "${aws_security_group.kafka-sg.id}"]
  associate_public_ip_address = true
  source_dest_check = false
  tags {
    Name = "kafka"
  }
  provisioner "remote-exec" {
    inline = [
      "docker run -d --name zookeeper --network host -e 'ALLOW_ANONYMOUS_LOGIN=yes' -e 'ZOO_HEAP_SIZE=256' -v zookeeper-data:/bitnami/zookeeper --restart always bitnami/zookeeper:latest",
      "sleep 8",
      "docker run -d --name kafka --network host -e 'KAFKA_ZOOKEEPER_CONNECT=localhost:2181' -e 'ALLOW_PLAINTEXT_LISTENER=yes' -v kafka-data:/bitnami/kafka --restart always bitnami/kafka:latest"
    ]
  }
  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = "${file("~/.ssh/diploma/id_rsa")}"
  }
}

resource "aws_instance" "vault" {
  ami  = "ami-02d2e9a2fd837fd50"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.diploma-key.id}"
  subnet_id = "${aws_subnet.public-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.diploma-public-sg.id}"]
  associate_public_ip_address = true
  source_dest_check = false
  tags {
    Name = "vault"
  }
  provisioner "remote-exec" {
    inline = [
      "docker run -d --name vault.diploma --restart=always --cap-add=IPC_LOCK -e 'VAULT_DEV_ROOT_TOKEN_ID=vaultToken1234' -e 'VAULT_DEV_LISTEN_ADDRESS=0.0.0.0:8200' -e 'VAULT_ADDR=http://127.0.0.1:8200' -p 80:8200 vault",
      "sleep 5",
      "docker exec -it vault.diploma sh -c 'vault login token=vaultToken1234; vault secrets disable secret; vault secrets enable -version=1 -path=secret kv; vault kv put secret/question-service database-password=question_service; vault kv put secret/messaging-service slack-webhook-url=https://hooks.slack.com/services/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'"
    ]
  }
  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = "${file("~/.ssh/diploma/id_rsa")}"
  }
}
