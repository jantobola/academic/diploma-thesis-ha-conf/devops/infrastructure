output "question-service IP public" {
  value = "${aws_instance.question-service.public_ip}"
}
output "question-service DNS public" {
  value = "${aws_route53_record.qs-dns-a-rec.name}"
}
output "question-service DNS local" {
  value = "${aws_route53_record.qs-dns-local-a-rec.name}"
}

output "messaging-service IP public" {
  value = "${aws_instance.messaging-service.public_ip}"
}
output "messaging-service DNS public" {
  value = "${aws_route53_record.ms-dns-a-rec.name}"
}
output "messaging-service DNS local" {
  value = "${aws_route53_record.ms-dns-local-a-rec.name}"
}

output "config-server IP public" {
  value = "${aws_instance.config-server.public_ip}"
}
output "config-server DNS public" {
  value = "${aws_route53_record.cs-dns-a-rec.name}"
}
output "config-server DNS local" {
  value = "${aws_route53_record.cs-dns-local-a-rec.name}"
}

output "eureka IP public" {
  value = "${aws_instance.eureka.public_ip}"
}
output "eureka DNS public" {
  value = "${aws_route53_record.eureka-dns-a-rec.name}"
}
output "eureka DNS local" {
  value = "${aws_route53_record.eureka-dns-local-a-rec.name}"
}

output "sba IP public" {
  value = "${aws_instance.sba.public_ip}"
}
output "sba DNS public" {
  value = "${aws_route53_record.sba-dns-a-rec.name}"
}
output "sba DNS local" {
  value = "${aws_route53_record.sba-dns-local-a-rec.name}"
}

output "postgres IP" {
  value = "${aws_instance.postgres.public_ip}"
}
output "postgres DNS public" {
  value = "${aws_route53_record.postgres-dns-a-rec.name}"
}
output "postgres DNS local" {
  value = "${aws_route53_record.postgres-dns-local-a-rec.name}"
}

output "kafka IP" {
  value = "${aws_instance.kafka.public_ip}"
}
output "kafka DNS public" {
  value = "${aws_route53_record.kafka-dns-a-rec.name}"
}
output "kafka DNS local" {
  value = "${aws_route53_record.kafka-dns-local-a-rec.name}"
}

output "vault IP" {
  value = "${aws_instance.vault.public_ip}"
}
output "vault DNS public" {
  value = "${aws_route53_record.vault-dns-a-rec.name}"
}
output "vault DNS local" {
  value = "${aws_route53_record.vault-dns-local-a-rec.name}"
}