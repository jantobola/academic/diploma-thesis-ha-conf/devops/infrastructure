// public zone is not managed by Terraform
// just manage particulat records

data "aws_route53_zone" "mockerize-cloud" {
  name = "mockerize.cloud."
  private_zone = false
}

// Service Discovery
resource "aws_route53_record" "eureka-dns-a-rec" {
  zone_id = "${data.aws_route53_zone.mockerize-cloud.id}"
  name = "eureka.mockerize.cloud"
  type = "A"
  ttl = "1500"
  records = ["${aws_instance.eureka.public_ip}"]
}

// Config Server
resource "aws_route53_record" "cs-dns-a-rec" {
  zone_id = "${data.aws_route53_zone.mockerize-cloud.id}"
  name = "cs.mockerize.cloud"
  type = "A"
  ttl = "1500"
  records = ["${aws_instance.config-server.public_ip}"]
}

// SBA
resource "aws_route53_record" "sba-dns-a-rec" {
  zone_id = "${data.aws_route53_zone.mockerize-cloud.id}"
  name = "sba.mockerize.cloud"
  type = "A"
  ttl = "1500"
  records = ["${aws_instance.sba.public_ip}"]
}

// Question Service
resource "aws_route53_record" "qs-dns-a-rec" {
  zone_id = "${data.aws_route53_zone.mockerize-cloud.id}"
  name = "mockerize.cloud"
  type = "A"
  ttl = "1500"
  records = ["${aws_instance.question-service.public_ip}"]
}

// Messaging Service
resource "aws_route53_record" "ms-dns-a-rec" {
  zone_id = "${data.aws_route53_zone.mockerize-cloud.id}"
  name = "ms.mockerize.cloud"
  type = "A"
  ttl = "1500"
  records = ["${aws_instance.messaging-service.public_ip}"]
}

// Kafka
resource "aws_route53_record" "kafka-dns-a-rec" {
  zone_id = "${data.aws_route53_zone.mockerize-cloud.id}"
  name = "kafka.mockerize.cloud"
  type = "A"
  ttl = "1500"
  records = ["${aws_instance.kafka.public_ip}"]
}

// Vault
resource "aws_route53_record" "vault-dns-a-rec" {
  zone_id = "${data.aws_route53_zone.mockerize-cloud.id}"
  name = "vault.mockerize.cloud"
  type = "A"
  ttl = "1500"
  records = ["${aws_instance.vault.public_ip}"]
}

// Postgres
resource "aws_route53_record" "postgres-dns-a-rec" {
  zone_id = "${data.aws_route53_zone.mockerize-cloud.id}"
  name = "postgres.mockerize.cloud"
  type = "A"
  ttl = "1500"
  records = ["${aws_instance.postgres.public_ip}"]
}

// -----------------------------------------------------------
// Private zone

resource "aws_route53_zone" "private" {
  name = "mockerize-local.cloud"

  vpc {
    vpc_id = "${aws_vpc.diploma.id}"
  }
}

// Service Discovery
resource "aws_route53_record" "eureka-dns-local-a-rec" {
  zone_id = "${aws_route53_zone.private.id}"
  name = "eureka.mockerize-local.cloud"
  type = "A"
  ttl = "300"
  records = ["${aws_instance.eureka.private_ip}"]
}

// Config Server
resource "aws_route53_record" "cs-dns-local-a-rec" {
  zone_id = "${aws_route53_zone.private.id}"
  name = "cs.mockerize-local.cloud"
  type = "A"
  ttl = "300"
  records = ["${aws_instance.config-server.private_ip}"]
}

// SBA
resource "aws_route53_record" "sba-dns-local-a-rec" {
  zone_id = "${aws_route53_zone.private.id}"
  name = "sba.mockerize-local.cloud"
  type = "A"
  ttl = "300"
  records = ["${aws_instance.sba.private_ip}"]
}

// Question Service
resource "aws_route53_record" "qs-dns-local-a-rec" {
  zone_id = "${aws_route53_zone.private.id}"
  name = "qs.mockerize-local.cloud"
  type = "A"
  ttl = "300"
  records = ["${aws_instance.question-service.private_ip}"]
}

// Messaging Service
resource "aws_route53_record" "ms-dns-local-a-rec" {
  zone_id = "${aws_route53_zone.private.id}"
  name = "ms.mockerize-local.cloud"
  type = "A"
  ttl = "300"
  records = ["${aws_instance.messaging-service.private_ip}"]
}

// Kafka
variable "kafka-local-dns" {
  default = "kafka.mockerize-local.cloud"
}
resource "aws_route53_record" "kafka-dns-local-a-rec" {
  zone_id = "${aws_route53_zone.private.id}"
  name = "${var.kafka-local-dns}"
  type = "A"
  ttl = "300"
  records = ["${aws_instance.kafka.private_ip}"]
}

// Vault
resource "aws_route53_record" "vault-dns-local-a-rec" {
  zone_id = "${aws_route53_zone.private.id}"
  name = "vault.mockerize-local.cloud"
  type = "A"
  ttl = "300"
  records = ["${aws_instance.vault.private_ip}"]
}

// Postgres
resource "aws_route53_record" "postgres-dns-local-a-rec" {
  zone_id = "${aws_route53_zone.private.id}"
  name = "postgres.mockerize-local.cloud"
  type = "A"
  ttl = "300"
  records = ["${aws_instance.postgres.private_ip}"]
}